* WARNING *

Following much-needed "housecleaning", the dm3_lib repository has been updated and moved to GitHub:
** https://github.com/piraynal/pyDM3reader **

The BitBucket repository is no longer updated and will be deactivated soon.
